function showTableData() {
    document.getElementById('info').innerHTML = "";
    var myTab = document.getElementById('table');

    // LOOP THROUGH EACH ROW OF THE TABLE AFTER HEADER.
    for (i = 1; i < myTab.rows.length; i++) {

        // GET THE CELLS COLLECTION OF THE CURRENT ROW.
        var objCells = myTab.rows.item(i).cells;

        // LOOP THROUGH EACH CELL OF THE CURENT ROW TO READ CELL VALUES.
        for (var j = 0; j < objCells.length; j++) {
            info.innerHTML = info.innerHTML + ' ' + objCells.item(j).innerHTML;
        }
        info.innerHTML = info.innerHTML + '<br />';     // ADD A BREAK (TAG).
    }


}


function CreateTable() {

    // CREATE DYNAMIC TABLE.
    var table = document.createElement('table');

    // SET THE TABLE ID. 
    // WE WOULD NEED THE ID TO TRAVERSE AND EXTRACT DATA FROM THE TABLE.
    table.setAttribute('id', 'table');

    var arrHead = new Array();
    arrHead = ['Emp. ID', 'Emp.Name', 'Designation'];

    var arrValue = new Array();
    arrValue.push(['1', 'Green Field', 'Accountant']);
    arrValue.push(['2', 'Arun Banik', 'Project Manager']);
    arrValue.push(['3', 'Dewane Paul', 'Programmer']);

    var tr = table.insertRow(-1);

    for (var h = 0; h < arrHead.length; h++) {
        var th = document.createElement('th');              // TABLE HEADER.
        th.innerHTML = arrHead[h];
        tr.appendChild(th);
    }

    for (var c = 0; c <= arrValue.length - 1; c++) {
        tr = table.insertRow(-1);

        for (var j = 0; j < arrHead.length; j++) {
            var td = document.createElement('td');          // TABLE DEFINITION.
            td = tr.insertCell(-1);
            td.innerHTML = arrValue[c][j];                  // ADD VALUES TO EACH CELL.
        }
    }

    // NOW CREATE AN INPUT BOX TYPE BUTTON USING createElement() METHOD.
    var button = document.createElement('input');

    // SET INPUT ATTRIBUTE 'type' AND 'value'.
    button.setAttribute('type', 'button');
    button.setAttribute('value', 'Show Table Data');

    // ADD THE BUTTON's 'onclick' EVENT.
    button.setAttribute('onclick', 'showTableData()');

    // FINALLY ADD THE NEWLY CREATED TABLE AND BUTTON TO THE BODY.
    document.body.appendChild(table);
    document.body.appendChild(button);



}

function CreateTableFromJSON() {
    // CREATE DYNAMIC TABLE.
    var table = document.createElement("table");
    table.setAttribute('id', 'table');
    table.setAttribute('class', 'table table-striped table-dark table-hover ');

    var myBooks = [
        {
            "Book ID": "1",
            "Book Name": "The Grass is Always Greener",
            "Category": "Modern Times",
            "Price": "125.60",
            "Date": "3/1/2019 11:10"
        },
        {
            "Book ID": "2",
            "Book Name": "Murder",
            "Category": "Crime",
            "Price": "56.00",
            "Date": "9/7/2019 19:50"
        },
        {
            "Book ID": "3",
            "Book Name": "A Boy at Seven",
            "Category": "Romance",
            "Price": "56.00",
            "Date": "7/5/2019 12:21"
        },
        {
            "Book ID": "4",
            "Book Name": "The Signalman",
            "Category": "Adventure",
            "Price": "56.00",
            "Date": "6/2/2019 10:39"
        },
        {
            "Book ID": "5",
            "Book Name": "Popular Science",
            "Category": "Science",
            "Price": "210.40",
            "Date": "7/1/2019 15:40"
        }
    ]

    // EXTRACT VALUE FOR HTML HEADER. 
   
    var col = [];
    for (var i = 0; i < myBooks.length; i++) {
        for (var key in myBooks[i]) {
            if (col.indexOf(key) === -1) {
                col.push(key);
            }
        }
    }

    
    // CREATE HTML TABLE HEADER ROW USING THE EXTRACTED HEADERS ABOVE.

    var tr = table.insertRow(-1);                   // TABLE ROW.

    for (var i = 0; i < col.length; i++) {
        var th = document.createElement("th");      // TABLE HEADER.
        th.innerHTML = col[i];
        tr.appendChild(th);
    }

    // ADD JSON DATA TO THE TABLE AS ROWS.
    for (var i = 0; i < myBooks.length; i++) {

        tr = table.insertRow(-1);

        for (var j = 0; j < col.length; j++) {
            var tabCell = tr.insertCell(-1);
            tabCell.innerHTML = myBooks[i][col[j]];
        }
    }

    // FINALLY ADD THE NEWLY CREATED TABLE WITH JSON DATA TO A CONTAINER.
    var divContainer = document.getElementById("showData");

    divContainer.innerHTML = "";
    divContainer.appendChild(table);
}

function myFunction() {
    var x = document.getElementById("form");
    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
}




function insRow(id) {
    var x = document.getElementById(id).insertRow(table.rows.length);
    var bookId = x.insertCell(0);
    var bookName = x.insertCell(1);
    var category = x.insertCell(2);
    var price = x.insertCell(3);
    var date = x.insertCell(4);
    bookId.innerHTML = table.rows.length - 1;
    bookName.innerHTML = document.getElementById("btn2").value;
    category.innerHTML = document.getElementById("btn3").value;
    price.innerHTML = document.getElementById("btn4").value;
    n = new Date();
    y = n.getFullYear();
    m = n.getMonth() + 1;
    d = n.getDate();
    f = n.getHours();
    c = n.getMinutes();
    date.innerHTML=  m + "/" + d + "/" + y + " " + f + ":" + c;
}





